<?php

namespace Drupal\ikows_parser\Form;

use Drupal\file\Entity\File;
use Drupal\Core\Form\FormBase;
use Drupal\taxonomy\Entity\Term;
use function GuzzleHttp\json_decode;
use Drupal\Core\Form\FormStateInterface;
use Drupal\devel\Plugin\Devel\Dumper\VarDumper;

class IkowsForm extends FormBase
{
    public function getFormId()
    {
        return 'ikows_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['json'] = [
            '#type' => 'managed_file',
            '#title' => $this->t('Votre json'),
            '#upload_validators' => array(
                'file_validate_extensions' => array('json')
            )
        ];

        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Envoyer'),
            '#button_type' => 'primary'
        ];

        return $form;
    }

    public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state)
    {

            //Recup de l'info du formulaire
            $json = $form_state->getValue('json');
            $file = File::load( $json[0] );
            //Je sors le contenu json et le decode pour en faire un objet
            $handle = file_get_contents($file->getFileUri());
            $handle = json_decode($handle);

            //Creation d'un tableau contenant les pays
            $pays = [];
            foreach ($handle->features as $key => $value) {
                $pays += [
                    $value->properties->NAME => [
                    'ISO3' => $value->properties->ISO3,
                    'polygon' => $value->geometry->type,
                    'coordinates' => $value->geometry->coordinates
                ]
                ];
            }

            //creation de la taxonomie dans Pays
            foreach ($pays as $key => $value) {
                $term = Term::create(array(
                    'parent' => array(),
                    'name' => $key,
                    'vid' => 'pays',
                  ))->save();
            }
    }
}
