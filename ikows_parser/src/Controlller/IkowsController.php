<?php

namespace Drupal\ikows_parser\Controller;

use Drupal\Core\Controller\ControllerBase;

class IkowsController extends ControllerBase
{
    public function content()
    {
        $form = $this->formBuilder()->getForm('Drupal\ikows_parser\Form\IkowsForm');
        return $form;
    }
}
